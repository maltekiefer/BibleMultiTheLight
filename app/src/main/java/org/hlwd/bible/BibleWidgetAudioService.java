package org.hlwd.bible;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;

import java.util.ArrayList;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

public class BibleWidgetAudioService extends Service
{
    private Context _context = null;
    private int _step = 5;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        _context = getApplicationContext();

        Notify(_context);
    }

    private void Notify(final Context context)
    {
        try
        {
            //Not necessary for old Android
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) return;

            final String longContentText = "Please turn off this unused notification channel, mandatory for foreground services!";
            final String channelId = "thelight_widget_channel_id";
            final NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

            final Intent intentOpen = new Intent(context, MainActivity.class);
            intentOpen.putExtra("NOTIF_ID", 2);

            final PendingIntent pendingIntentOpen = PendingIntent.getActivity(
                    context,
                    0,
                    intentOpen,
                    PendingIntent.FLAG_CANCEL_CURRENT);

            final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, channelId)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_LOW)
                    .setVisibility(NotificationCompat.VISIBILITY_SECRET)
                    .setSmallIcon(R.drawable.thelightnotif)
                    .setContentTitle(context.getString(R.string.channelWidgetBibleName))
                    .setCategory(NotificationCompat.CATEGORY_STATUS)
                    .setColorized(true)
                    .setWhen(System.currentTimeMillis())
                    .setShowWhen(false)
                    .setContentIntent(pendingIntentOpen)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);

            notificationBuilder.setContentText(longContentText);
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(longContentText));

            final int channelImportance = NotificationManager.IMPORTANCE_LOW;
            final String channelName = context.getString(R.string.channelWidgetBibleName);
            final NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, channelImportance);
            notificationChannel.setDescription(channelName);
            //notificationChannel.setAllowBubbles(false);       //TODO NEXT: bug on my phone
            notificationChannel.setShowBadge(false);
            notificationChannel.enableLights(false);
            notificationChannel.enableVibration(false);

            Objects.requireNonNull(notificationManager).createNotificationChannel(notificationChannel);

            startForeground(2, notificationBuilder.build());

            final Handler hdl = new Handler();
            hdl.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        notificationManager.deleteNotificationChannel(channelId);
                    }
                    catch(Exception ex)
                    {
                        if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
                    }
                }
            },500);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        try
        {
            this.startForegroundService(intent);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }

        return START_NOT_STICKY;
    }

    @Override
    public ComponentName startForegroundService(Intent intent)
    {
        try
        {
            final SCommon _s = CommonWidget.CheckLocalInstance(_context);
            final String listenPosition = _s.SayStop();
            if (listenPosition == null) return null;

            final String[] arr =  listenPosition.split(",");
            if (arr.length < 4) return null;

            String bbName = arr[0];
            final int bNumber = Integer.parseInt(arr[1]);
            int cNumber = Integer.parseInt(arr[2]);
            int vNumber = Integer.parseInt(arr[3]);

            //*** Get params
            final int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);

            final String action = intent.getAction();
            switch (Objects.requireNonNull(action))
            {
                case "WIDGET_STOP_CLICK":
                {
                    break;
                }
                case "WIDGET_PLAY_CLICK":
                {
                    _s.Say();

                    break;
                }
                case "WIDGET_START_CLICK":
                case "WIDGET_BACK_CLICK":
                case "WIDGET_FORWARD_CLICK":
                case "WIDGET_END_CLICK":
                case "WIDGET_LANG_CLICK":
                {
                    if (Objects.requireNonNull(action).equalsIgnoreCase("WIDGET_START_CLICK"))
                    {
                        if (cNumber - 1 < 1)
                        {
                            cNumber = 1;
                        }
                        else
                        {
                            cNumber--;
                        }

                        vNumber = 1;

                        _s.SaySetCurrentVerseNumber(vNumber);
                    }
                    else if (Objects.requireNonNull(action).equalsIgnoreCase("WIDGET_BACK_CLICK"))
                    {
                        vNumber = vNumber - _step;
                        if (vNumber < 1) vNumber = 1;

                        _s.SaySetCurrentVerseNumber(vNumber);
                    }
                    else if (Objects.requireNonNull(action).equalsIgnoreCase("WIDGET_FORWARD_CLICK"))
                    {
                        vNumber = vNumber + _step;

                        final int vCount = _s.GetChapterVerseCount(bNumber, cNumber);

                        if (vNumber > vCount)
                        {
                            vNumber = vCount;
                        }

                        _s.SaySetCurrentVerseNumber(vNumber);
                    }
                    else if (Objects.requireNonNull(action).equalsIgnoreCase("WIDGET_LANG_CLICK"))
                    {
                        bbName = CommonWidget.RollBookName(_context, bbName);
                    }
                    else  //NEXT
                    {
                        final int cNumberMax = _s.GetBookChapterMax(bNumber);

                        if (cNumber + 1 > cNumberMax)
                        {
                            return null;
                        }
                        else
                        {
                            cNumber++;
                            vNumber = 1;

                            _s.SaySetCurrentVerseNumber(vNumber);
                        }
                    }

                    _s.SetListenPosition(_context, bbName, bNumber, cNumber, vNumber);

                    break;
                }
            }

            //*** Update widget
            final ArrayList<VerseBO> arrVerse = _s.GetVerse(bbName, bNumber, cNumber, vNumber);
            final VerseBO verse = arrVerse.size() == 0 ? new VerseBO(): arrVerse.get(0);
            final String verseRef = PCommon.ConcaT("~", verse.bsName, " ", verse.cNumber, ".", verse.vNumber);

            CommonWidgetAudio.UpdateAppWidget(_context, appWidgetId, bbName, verseRef);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }

        return null;
    }
}
