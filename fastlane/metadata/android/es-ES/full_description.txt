Biblia en varios idiomas, gratuita offline, sin publicidad, integramente en Inglés, francés, italiano, español, portugués.

King James Version, Segond, Diodati, Valera, Almeida, Ostervald.

Fácil de usar con funciones de búsqueda rápida y compartir, planes de lectura, Biblia audio, artículos, referencias cruzadas, widgets.

También funciona en Android TV, Chromebook.

