Bible multi languages, free, offline, no advertising, completely in English, French, Italian, Spanish, Portuguese.

King James Version, Segond, Diodati, Valera, Almeida, Ostervald.

Easy to use with quick search and share, plans of reading, audio Bible, articles, cross-references, widgets.

Also works on Android TV, Chromebook.